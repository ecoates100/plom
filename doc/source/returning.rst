.. Plom documentation
   Copyright 2022 Colin B. Macdonald
   SPDX-License-Identifier: AGPL-3.0-or-later


Returning Work to Students
==========================

.. note::

   Stub: move and/or write documentation.


Technical docs
--------------

* The command-line tool :doc:`plom-finish` is the current front-end
  for most tasks related to returning work.

* For scripting or other advanced usage, you can ``import plom.finish``
  in your own Python code.  See :doc:`module-plom-finish`.
