# Copyright (C) 2018-2020 Andrew Rechnitzer
# Copyright (C) 2020-2022 Colin B. Macdonald
# SPDX-License-Identifier: AGPL-3.0-or-later

__version__ = "0.9.3.dev"
# Also hardcoded in AppImageBuilder.yml
